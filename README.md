# Semana 14 - Ejemplo 2

### Relaciones

1. Preguntas Introductorias
2. Conceptos de Base de datos
3. Relaciones a nivel de base de datos
4. Relaciones N a N
5. Creando un segundo modelo
6. Agregando relaciones a los modelos

### Formularios y relaciones

1. Formularios con asociación
2. Formulario anidado
3. Intro a rutas anidadas
4. New y Create anidado
5. Index anidado
6. Nested Destroy
